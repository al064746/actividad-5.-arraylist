
package actividad.pkg5_.arraylist;

import java.util.ArrayList;

public class Programa_1 {
    
public static ArrayList<Integer> alColores(ArrayList<Integer> aLista){
        aLista.set(0, Math.abs(aLista.get(0)-255));
        aLista.set(1, Math.abs(aLista.get(1)-255));
        aLista.set(2, Math.abs(aLista.get(2)-255));
        //System.out.println(aLista);   
        return aLista; 
    }
    
    public static void main(String[] args) throws Exception {
             ArrayList<Integer> alColorActual    = new ArrayList<Integer>();
       ArrayList<Integer> alColorContraste = new ArrayList<Integer>();
       alColorActual.add(20);
       alColorActual.add(24);
       alColorActual.add(130);
       System.out.println(alColorActual);    
       alColorContraste = alColores(alColorActual);
       System.out.println(alColorContraste);   
 
    }
    }
    

